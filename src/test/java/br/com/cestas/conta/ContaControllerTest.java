package br.com.cestas.conta;

import com.google.gson.Gson;
import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import javax.ws.rs.client.WebTarget;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * Created by luismoro on 11/04/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContaControllerTest extends TestCase{

    String host = "http://localhost";
    String porta = ":8090";
    public WebTarget target;
    Gson gson = new Gson();
    HttpClient client;
    static String codigo;
    StringEntity inputConta;
    StringEntity inputContaAtualizar;

    @Before
    public void setUp() throws UnsupportedEncodingException {
        client = HttpClientBuilder.create().build();

        inputConta = new StringEntity("{\n" +
                "\t\"usuario\" : \"adminTeste\",\n" +
                "    \"email\" : \"admin@admin.com\",\n" +
                "    \"senha\" : \"adminTeste\"\n" +
                "}");

        inputContaAtualizar = new StringEntity("{\n" +
                "\t\"usuario\" : \"adminTesteAtualizado\",\n" +
                "    \"email\" : \"admin@admin.com\",\n" +
                "    \"senha\" : \"adminTeste\",\n" +
                "    \"id\" : \"" + codigo  + "\"\n" +
                "}");
    }

    @Test
    public void testACreate() throws Exception {

        HttpPost request = new HttpPost(host + porta + "/api/conta");

        request.addHeader("Content-Type", "application/json");
        inputConta.setContentType("application/json");
        request.setEntity(inputConta);

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals("adminTeste", stringObjectMap.get("usuario"));
    }

    @Test
    public void testBFindByUsuario() throws Exception {
        HttpGet request = new HttpGet(host + porta + "/api/conta/usuario/adminTeste");
        request.addHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals("adminTeste", stringObjectMap.get("usuario"));

        codigo = (String) stringObjectMap.get("id");
    }

    @Test
    public void testCFindAll() throws Exception {
        HttpGet request = new HttpGet(host + porta + "/api/conta/");
        request.addHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        List stringObjectMap = gson.fromJson(resp, List.class);

        assertEquals(200, response.getStatusLine().getStatusCode());

        boolean r;
        if(stringObjectMap.size() > 0)
            r = true;
        else
            r = false;

        assertTrue(r);
    }

    @Test
    public void testDUpdate() throws Exception {
        HttpPut request = new HttpPut(host + porta + "/api/conta/" + codigo);

        request.addHeader("Content-Type", "application/json");
        inputConta.setContentType("application/json");
        request.setEntity(inputContaAtualizar);

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());

        codigo = (String) stringObjectMap.get("id");
    }

    @Test
    public void testEFindOne() throws Exception {
        HttpGet request = new HttpGet(host + porta + "/api/conta/" + codigo);
        request.addHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals("adminTesteAtualizado", stringObjectMap.get("usuario"));
    }

//    @Test
//    public void testFFindByEmail() throws Exception {
//        HttpGet request = new HttpGet(host + porta + "/api/conta/email/admin@admin.com");
//        request.addHeader("Content-Type", "application/json");
//
//        HttpResponse response = client.execute(request);
//
//        System.out.println("Response : "
//                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());
//
//        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));
//
//        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);
//
//        assertEquals(200, response.getStatusLine().getStatusCode());
//        assertEquals("adminTesteAtualizado", stringObjectMap.get("usuario"));
//
//        codigo = (String) stringObjectMap.get("_id");
//
//    }

    @Test
    public void testGDelete() throws Exception {
        HttpDelete request = new HttpDelete(host + porta + "/api/conta/" + codigo);

        request.addHeader("Content-Type", "application/json");
        inputConta.setContentType("application/json");

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }
}