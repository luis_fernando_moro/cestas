package br.com.cestas.cesta;

import com.google.gson.Gson;
import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import javax.ws.rs.client.WebTarget;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * Created by luismoro on 12/04/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CestaControllerTest extends TestCase {

    String host = "http://localhost";
    String porta = ":8090";
    String endpoint = "/api/cesta/";
    public WebTarget target;
    Gson gson = new Gson();
    HttpClient client;
    static String codigo;
    StringEntity inputCesta;
    StringEntity inputCestaAtualizar;

    @Before
    public void setUp() throws UnsupportedEncodingException {
        client = HttpClientBuilder.create().build();

        inputCesta = new StringEntity("{\n" +
                "\t\"foto\" : \"foto\",\n" +
                "    \"descricao\" : \"descricao\",\n" +
                "    \"nome\" : \"nome\",\n" +
                "    \"preco\" : 5.5\n" +
                "}");

        inputCestaAtualizar = new StringEntity("{\n" +
                "\t\"foto\" : \"fotoAtualizada\",\n" +
                "    \"descricao\" : \"descricaoAtualizada\",\n" +
                "    \"preco\" : 5.5\n," +
                "    \"nome\" : \"nomeAtualizado\",\n" +
                "    \"id\" : \"" + codigo  + "\"\n" +
                "}");
    }

    @Test
    public void testACreate() throws Exception {
        HttpPost request = new HttpPost(host + porta + endpoint);

        request.addHeader("Content-Type", "application/json");
        inputCesta.setContentType("application/json");
        request.setEntity(inputCesta);

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals("descricao", stringObjectMap.get("descricao"));
    }

    @Test
    public void testBFindByDescricao() throws Exception {
        HttpGet request = new HttpGet(host + porta + endpoint + "descricao/descricao");
        request.addHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals("descricao", stringObjectMap.get("descricao"));

        codigo = (String) stringObjectMap.get("id");
    }

    public void testCFindAll() throws Exception {
        HttpGet request = new HttpGet(host + porta + endpoint);
        request.addHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        List stringObjectMap = gson.fromJson(resp, List.class);

        assertEquals(200, response.getStatusLine().getStatusCode());

        boolean r;
        if(stringObjectMap.size() > 0)
            r = true;
        else
            r = false;

        assertTrue(r);
    }

    @Test
    public void testDUpdate() throws Exception {
        HttpPut request = new HttpPut(host + porta + endpoint + codigo);

        request.addHeader("Content-Type", "application/json");
        request.setEntity(inputCestaAtualizar);

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());

        codigo = (String) stringObjectMap.get("id");
    }

    @Test
    public void testEFindOne() throws Exception {
        HttpGet request = new HttpGet(host + porta + endpoint + codigo);
        request.addHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals("fotoAtualizada", stringObjectMap.get("foto"));
    }

//    @Test
//    public void testFindByItens() throws Exception {
//        HttpGet request = new HttpGet(host + porta + "/api/cesta/itens/");
//        request.addHeader("Content-Type", "application/json");
//
//        HttpResponse response = client.execute(request);
//
//        System.out.println("Response : "
//                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());
//
//        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));
//
//        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);
//
//        assertEquals(200, response.getStatusLine().getStatusCode());
//        assertEquals("", stringObjectMap.get("itens"));
////    }


    @Test
    public void testGFindByPreco() throws Exception {
        HttpGet request = new HttpGet(host + porta + endpoint + "preco/" + new Double(5.5));
        request.addHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + stringObjectMap);


        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals(5.5, stringObjectMap.get("preco"));
    }

    @Test
    public void testHFindByFoto() throws Exception {
        HttpGet request = new HttpGet(host + porta + endpoint + "foto/fotoAtualizada");
        request.addHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals("fotoAtualizada", stringObjectMap.get("foto"));

        codigo = (String) stringObjectMap.get("id");
    }

    @Test
    public void testIFindByNome() throws Exception {
        HttpGet request = new HttpGet(host + porta + endpoint + "nome/nomeAtualizado");
        request.addHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());
        assertEquals("nomeAtualizado", stringObjectMap.get("nome"));
    }

    @Test
    public void testJDelete() throws Exception {
        HttpDelete request = new HttpDelete(host + porta + endpoint + codigo);

        request.addHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);

        System.out.println("Response : "
                + response.getStatusLine().getStatusCode() + " - " + response.getEntity().getContent());

        String resp = new String(IOUtils.toByteArray(response.getEntity().getContent()));

        Map<String,Object> stringObjectMap = gson.fromJson(resp, Map.class);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }
}