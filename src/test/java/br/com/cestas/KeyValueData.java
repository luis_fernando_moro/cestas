package br.com.cestas;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by luismoro on 11/04/16.
 */
public class KeyValueData implements Serializable {

    private static final long serialVersionUID = -2425647697031907018L;

    @JsonProperty("key")
    public String key;

    @JsonProperty("value")
    public String value;
}
