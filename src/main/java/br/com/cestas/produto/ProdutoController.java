package br.com.cestas.produto;

import br.com.cestas.commons.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by LuisMoro on 10/04/16.
 */
@RestController
@RequestMapping(value= Constants.API + "produto", produces = MediaType.APPLICATION_JSON_VALUE)

public class ProdutoController {
	
	@Autowired
	private ProdutoRepository produtoRepository;

	@RequestMapping(method=RequestMethod.GET)
	public Iterable<Produto> findAll(){
		return this.produtoRepository.findAll();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Produto findOne(@PathVariable(value = "id") String id){
		return this.produtoRepository.findOne(id);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Produto create(@RequestBody Produto produto){
		return this.produtoRepository.save(produto);
	}
	
	@RequestMapping(value="/{id}", method={RequestMethod.PUT, RequestMethod.PATCH})
	public Produto update(@PathVariable(value = "id") String id, @RequestBody Produto produto){
		return this.produtoRepository.save(produto);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void delete(@PathVariable(value="id") String id ){
		Produto produto = this.produtoRepository.findOne(id);
		if ( produto != null ){
			this.produtoRepository.delete(produto);
		}
	}

	@RequestMapping(value="/nome/{nome}", method=RequestMethod.GET)
	public Produto findByName(@PathVariable(value = "nome") String nome){
		return this.produtoRepository.findByNome(nome);
	}

	@RequestMapping(value="/categoria/{categoria}", method=RequestMethod.GET)
	public Produto findByCategory(@PathVariable(value = "categoria") String categoria){
		return this.produtoRepository.findByCategoria(categoria);
	}
}
