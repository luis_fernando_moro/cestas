package br.com.cestas.produto;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by LuisMoro on 10/04/16.
 */
public interface ProdutoRepository extends MongoRepository<Produto, String> {
    public Produto findByCategoria(String categoria);
    public Produto findByNome(String nome);

}