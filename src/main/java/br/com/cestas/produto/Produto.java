package br.com.cestas.produto;

import br.com.cestas.commons.DomainEntity;

import javax.persistence.Entity;

/**
 * Created by LuisMoro on 10/04/16.
 */

@Entity
public class Produto extends DomainEntity {

	public String nome;
	public String categoria;

	@Override
	public String toString() {
		return String.format(
				"Customer[id=%s, nome='%s', categoria='%s']",
				id, nome, categoria);
	}
}