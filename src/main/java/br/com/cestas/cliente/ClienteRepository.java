package br.com.cestas.cliente;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by root on 10/12/15.
 */
public interface ClienteRepository extends MongoRepository<Cliente, String> {
    public Cliente findByNome(List<String> itens);
}
