package br.com.cestas.cliente;

import br.com.cestas.commons.DomainEntity;

import javax.persistence.Entity;

/**
 * Created by luismoro on 09/04/2016.
 */

@Entity
public class Cliente extends DomainEntity {

    public String nome;
    public String cpf;

    @Override
    public String toString() {
        return String.format(
                "Cliente[id=%s, nome='%s', cpf='%s']",
                id, nome, cpf);
    }

}
