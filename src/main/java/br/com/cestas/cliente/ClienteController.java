package br.com.cestas.cliente;

import br.com.cestas.commons.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by luismoro on 09/04/2016.
 */

@RestController
@RequestMapping(value= Constants.API + "cliente", produces = MediaType.APPLICATION_JSON_VALUE)
public class ClienteController {

    @Autowired
    private ClienteRepository clienteRepository;

    @RequestMapping(method=RequestMethod.GET)
    public List<Cliente> findAll(){
        return this.clienteRepository.findAll();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Cliente findOne(@PathVariable(value = "id") String id){
        return this.clienteRepository.findOne(id);
    }

    @RequestMapping(method=RequestMethod.POST)
    public Cliente create(@RequestBody Cliente cliente){
        return this.clienteRepository.save(cliente);
    }

    @RequestMapping(value="/{id}", method={RequestMethod.PUT, RequestMethod.PATCH})
    public Cliente update(@PathVariable(value = "id") String id, @RequestBody Cliente cliente){
        return this.clienteRepository.save(cliente);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public void delete(@PathVariable(value="id") String id ){
        Cliente cliente = this.clienteRepository.findOne(id);
        if ( cliente != null ){
            this.clienteRepository.delete(cliente);
        }
    }

    @RequestMapping(value="/userName/{cliente}", method=RequestMethod.GET)
    public Cliente findByUsername(@PathVariable(value = "cliente") List<String> cliente){
        return this.clienteRepository.findByNome(cliente);
    }

}
