package br.com.cestas.conta;

import br.com.cestas.commons.DomainEntity;

import javax.persistence.Entity;

/**
 * Created by luismoro on 09/04/2016.
 */

@Entity
public class Conta extends DomainEntity {

    public String usuario;
    public String email;
    public String senha;

}
