package br.com.cestas.conta;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by root on 10/12/15.
 */
public interface ContaRepository extends MongoRepository<Conta, String> {
    public Conta findByUsuario (String usuario);
    public Conta findByEmail (String email);
}
