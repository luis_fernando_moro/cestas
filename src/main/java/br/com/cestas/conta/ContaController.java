package br.com.cestas.conta;

import br.com.cestas.commons.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by luismoro on 09/04/2016.
 */

@RestController
@RequestMapping(value= Constants.API + "conta", produces = MediaType.APPLICATION_JSON_VALUE)
public class ContaController {

    @Autowired
    private ContaRepository contaRepository;

    @RequestMapping(method=RequestMethod.GET)
    public List<Conta> findAll(){
        return this.contaRepository.findAll();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Conta findOne(@PathVariable(value = "id") String id){
        return this.contaRepository.findOne(id);
    }

    @RequestMapping(method=RequestMethod.POST)
    public Conta create(@RequestBody Conta conta){
        return this.contaRepository.save(conta);
    }

    @RequestMapping(value="/{id}", method={RequestMethod.PUT, RequestMethod.PATCH})
    public Conta update(@PathVariable(value = "id") String id, @RequestBody Conta conta){
        return this.contaRepository.save(conta);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public void delete(@PathVariable(value="id") String id ){
        Conta conta = this.contaRepository.findOne(id);
        if ( conta != null ){
            this.contaRepository.delete(conta);
        }
    }

    @RequestMapping(value="/usuario/{usuario}", method=RequestMethod.GET)
    public Conta findByUsuario(@PathVariable(value="usuario") String usuario ){
        return this.contaRepository.findByUsuario(usuario);
    }

    @RequestMapping(value="/email/{email}", method=RequestMethod.GET)
    public Conta findByEmail(@PathVariable(value="email") String email ){
        return this.contaRepository.findByEmail(email);
    }
}
