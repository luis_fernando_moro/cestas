package br.com.cestas.commons;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by luismoro on 09/04/2016.
 */
@MappedSuperclass
public class DomainEntity {

    public static final String UUID = "uuid";
    public static final String ACTIVE = "active";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public String id;

    @Column(name = "uuid", nullable = false, updatable = false)
    public String uuid = java.util.UUID.randomUUID().toString();

    @Column(name = "creation_date", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    public Date creationDate = new Date();

    @Column(name = "active", nullable = false)
    public boolean active = true;
}
