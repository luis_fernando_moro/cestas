package br.com.cestas;

import br.com.cestas.commons.Constants;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by renan on 16/12/15.
 */

@RestController
@RequestMapping(value= Constants.API + "auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AutenticacaoController {

    @RequestMapping(method= RequestMethod.GET)
    public String auth( ){
        return "SERVIÇO NÃO SEGURO";
    }

    /*@RequestMapping(method= RequestMethod.GET)
    public String logout(HttpServletRequest httpServletRequest){
        httpServletRequest.getSession().invalidate();
        return "Logout Realizado com Sucesso!!!";
    }*/

}
