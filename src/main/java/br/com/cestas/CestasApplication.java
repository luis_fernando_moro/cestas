package br.com.cestas;

import br.com.cestas.commons.Constants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by luismoro on 09/04/2016.
 */

@EnableAutoConfiguration
@ComponentScan(basePackages = { Constants.PACKAGE_BASE })
@Configuration
@EnableMongoRepositories
@SpringBootApplication
public class CestasApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CestasApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(CestasApplication.class, args);
    }

}