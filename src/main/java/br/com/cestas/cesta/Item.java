package br.com.cestas.cesta;

import br.com.cestas.commons.DomainEntity;
import br.com.cestas.produto.Produto;

import javax.persistence.Entity;

/**
 * Created by luismoro on 09/04/2016.
 */

@Entity
public class Item extends DomainEntity {

    public Produto produto;
    public int quantidade;

    @Override
    public String toString() {
        return String.format(
                "Item[id=%s, produto='%s', quantidade='%s']",
                id, produto, quantidade);
    }
}
