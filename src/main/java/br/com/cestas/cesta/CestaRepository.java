package br.com.cestas.cesta;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by root on 10/12/15.
 */
public interface CestaRepository extends MongoRepository<Cesta, String> {

    public Cesta findByItens(List<String> itens);
    public Cesta findByFoto(String foto);
    public Cesta findByDescricao(String descricao);
    public Cesta findByPreco(double preco);
    public Cesta findByNome(String nome);
}
