package br.com.cestas.cesta;

import br.com.cestas.commons.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luismoro on 09/04/2016.
 */

@RestController
@RequestMapping(value= Constants.API + "cesta", produces = MediaType.APPLICATION_JSON_VALUE)
public class CestaController {

    @Autowired
    private CestaRepository cestaRepository;

    @RequestMapping(method=RequestMethod.GET)
    public List<Cesta> findAll(){
        return this.cestaRepository.findAll();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Cesta findOne(@PathVariable(value = "id") String id){
        return this.cestaRepository.findOne(id);
    }

    @RequestMapping(method=RequestMethod.POST)
    public Cesta create(@RequestBody Cesta cesta){
        return this.cestaRepository.save(cesta);
    }

    @RequestMapping(value="/list", method=RequestMethod.POST)
    public List<Cesta> createList(@RequestBody List<Cesta> cestas){
        List<Cesta> cestaList = new ArrayList<>();
        for (Cesta cesta : cestas) {
            cestaList.add(this.cestaRepository.save(cesta));
        }
        return cestaList;
    }

    @RequestMapping(value="/{id}", method={RequestMethod.PUT, RequestMethod.PATCH})
    public Cesta update(@PathVariable(value = "id") String id, @RequestBody Cesta cesta){
        return this.cestaRepository.save(cesta);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public void delete(@PathVariable(value="id") String id ){
        Cesta cesta = this.cestaRepository.findOne(id);
        if ( cesta != null ){
            this.cestaRepository.delete(cesta);
        }
    }

    @RequestMapping(value="/itens/{itens}", method=RequestMethod.GET)
    public Cesta findByItens(@PathVariable(value="itens") List<String> itens ){
        return this.cestaRepository.findByItens(itens);
    }

    @RequestMapping(value="/foto/{foto}", method=RequestMethod.GET)
    public Cesta findByFoto(@PathVariable(value="foto") String foto ){
        return this.cestaRepository.findByFoto(foto);
    }

    @RequestMapping(value="/preco/{preco:.+}", method=RequestMethod.GET)
    public Cesta findByPreco(@PathVariable(value="preco") Double preco ){
        return this.cestaRepository.findByPreco(preco);
    }

    @RequestMapping(value="/descricao/{descricao}", method=RequestMethod.GET)
    public Cesta findByDescricao(@PathVariable(value="descricao") String descricao ){
        return this.cestaRepository.findByDescricao(descricao);
    }

    @RequestMapping(value="/nome/{nome}", method=RequestMethod.GET)
    public Cesta findByNome(@PathVariable(value="nome") String nome ){
        return this.cestaRepository.findByNome(nome);
    }

    @RequestMapping(value="/principal", method=RequestMethod.GET)
    public List<Cesta> cestasPrincipal(){
        List<Cesta> cestas = this.cestaRepository.findAll();
        int mdc = mdc(cestas.size(), 6);

        List<Cesta> cestasNovo = new ArrayList<>();

        for (int i = 0; i < mdc; i++) {
            cestasNovo.add(cestas.get(i));
        }

        return cestasNovo;
    }

    @RequestMapping(value="/paginado/{qtd}/{pag}", method=RequestMethod.GET)
    public List<Cesta> cestasPaginado(@PathVariable(value="qtd") int qtd, @PathVariable(value="pag") int pag){
        List<Cesta> cestas = this.cestaRepository.findAll();
        int inicio = pag*qtd;
        int fim = inicio + qtd;

        if (fim > cestas.size()) {
            fim = cestas.size();
        }

        List<Cesta> cestasNovo = new ArrayList<>();

        for (int i = inicio; i < fim; i++) {
            cestasNovo.add(cestas.get(i));
        }

        return cestasNovo;
    }

    public int mdc(int a, int b){
        int resto = 1;
        int ret = b;

        while(resto != 0){
            ret = a;
            resto = a % b;
            a--;
        }

        return ret;
    }

    @RequestMapping(value="/paginado", method=RequestMethod.GET)
    public Page<Cesta> cestasPaginadoV(Pageable pageable) {
        return this.cestaRepository.findAll(pageable);
    }
}
