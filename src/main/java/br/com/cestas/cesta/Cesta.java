package br.com.cestas.cesta;

import br.com.cestas.commons.DomainEntity;

import javax.persistence.Entity;
import java.util.List;

/**
 * Created by luismoro on 09/04/2016.
 */

@Entity
public class Cesta extends DomainEntity {

    public List<Item> itens;
    public String foto;
    public String descricao;
    public double preco;
    public String nome;

    @Override
    public String toString() {
        return String.format(
                "Cesta[id=%s, itens='%s', foto=%s, descricao=%s, preco=%d, nome=%s,]",
                id, itens, foto, descricao, preco, nome);
    }
}
