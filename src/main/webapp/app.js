/**
 * Created by luismoro on 14/04/16.
 */
'use strict';

var cestaApp = angular.module('cestaApp', ['ngRoute', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngAnimate']);

var server = "localhost";

var porta = "8092"

cestaApp.config(function($routeProvider) {

    $routeProvider.
        when('/', {
            templateUrl: 'principal.html'
        }).
        when('/cesta', {
            templateUrl: 'cestas.html'
        }).
        when('/sobre', {
            templateUrl: 'sobre.html'
        })
        .when('/contato', {
            templateUrl: 'contato.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});

cestaApp.controller('CestaCtrl', function($rootScope, $location, $http, $scope) {
    $rootScope.activetab = $location.path();
    $scope.todos = []
    $http({
        method : "GET",
        //url : "http://" + server + ":" + porta + "/api/cesta"
        url : "/api/cesta"
    }).then(function mySucces(response) {
        $scope.cestas = response.data;
    }, function myError(response) {
        $scope.cestas = response.statusText;
    });


    $http({
        method : "GET",
        url : "/api/cesta/principal"
    }).then(function mySucces(response) {
        $scope.cestasPrincipal = response.data;
    }, function myError(response) {
        $scope.cestasPrincipal = response.statusText;
    });

    $scope.setCaminhoImagem = function(cam, nome) {
        document.getElementById("myModalLabel").textContent = "Cesta " + nome;
        document.getElementById("myModalLabel").style.color = "cadetblue";
        document.getElementById("imagemModal").src = cam;
    };
});

function MyController($location, $rootScope, $http, $scope) {

    $scope.currentPage = 1;
    $scope.pageSize = 6;
    $scope.meals = [];

    $rootScope.activetab = $location.path();
    $scope.todos = [];

    $http({
        method : "GET",
        //url : "http://" + server + ":" + porta + "/api/cesta"
        url : "/api/cesta"
    }).then(function mySucces(response) {
        $scope.cestas = response.data;
    }, function myError(response) {
        $scope.cestas = response.statusText;
    });

    $scope.pageChangeHandler = function(num) {
        console.log('meals page changed to ' + num);
    };

    $scope.setCaminhoImagem = function(cam, nome) {
        document.getElementById("myModalLabel").textContent = "Cesta " + nome;
        document.getElementById("myModalLabel").style.color = "cadetblue";
        document.getElementById("imagemModal").src = cam;
    };
}

function OtherController($scope) {
    $scope.pageChangeHandler = function(num) {
        console.log('going to page ' + num);
    };
}

cestaApp.controller('MyController', MyController);
cestaApp.controller('OtherController', OtherController);